---
layout: default
---

{% include manifest.md %}

[Manifest en format pdf](ManifestFP.pdf)

[Manifiesto en Castellano (pdf)](ManifiestoFP.pdf)

# Adherir-te a aquest manifest

Si ets professora o professor, i vols adherir-te a aquest manifest, pots signar-lo a (atenció: has d'entrar amb el teu usuari xtec):
[https://forms.gle/s69dCq14R9DJ75Eu8](https://forms.gle/s69dCq14R9DJ75Eu8){:target="_blank"}

<a href ="https://forms.gle/s69dCq14R9DJ75Eu8" target="_blank">
 <button type="button">Adhereix-te</button>
</a>


### Error: "Permís obligatori"
Si et surt l'error de "Permís obligatori", ves primer al següent enllaç i entra amb el teu usuari xtec.


[https://mail.google.com/a/xtec.cat/](https://mail.google.com/a/xtec.cat/){:target="_blank"}

Un cop hagis entrat torna a provar de signar el manifest:


[https://forms.gle/s69dCq14R9DJ75Eu8](https://forms.gle/s69dCq14R9DJ75Eu8){:target="_blank"}

# Amb el suport de

<div class="sindicats">
	<img alt="USTEC" src="img/ustec.jpg" class="widelogo"/>
	<img alt="ASPEPC" src="img/aspepc.png" class="widelogo"/>
	<img alt="CGT" src="img/cgt.png" class="longlogo"/>
	<img alt="intersindical" src="img/intersindical.jpeg" class="widelogo"/>
	<img alt="UGT" src="img/ugt.jpg"  class="longlogo"/>
	<img alt="CCOO" src="img/ccoo.png" class="widelogo"/>
</div>

{% include press.html %}
