# MANIFEST CONTRA LA REFORMA ​LOW-COST DE L’FP

Davant la reforma curricular express de la FP prevista a Catalunya per ser
aplicada el curs vinent i sorpresivament anunciada pel Departament
d’Educació en les darreres setmanes, **la
comunitat docent i sindical llencem el següent manifest**:

1. **No
s’ha comptat amb la opinió del professorat**
per quins són els continguts a reduir o mantenir, ni quines
modificacions s’haurien d’incorporar. Dubtem que la premura per
implantar aquesta reforma, la manca de transparència en el procés,
i l’absència de debat entre els i les responsables del
desenvolupament dels currículums d’una banda, i professors i
professores amb implicació en el desplegament d’una altra, hagi
permès la suficient profunditat d’anàlisi que porti a la
impartició dels temaris adequats que requereix el nostre alumnat. 

2. La reducció d’hores lectives suposa una **merma
de la qualitat de la formació** per
a l’alumne, ja amb prous carències de coneixements, i escurça
cada cop més del seu temps de preparació enfront de la que serà la
seva llarga futura vida laboral i personal.

3. Es produeix un **buidat
en els continguts específics**
de cada ofici que fa que l’alumnat arribi a l’empresa amb menys
coneixements elementals per desenvolupar la seva carrera, i posa més
difícil que es converteixi en un treballador qualificat amb
possibilitats de progressar en el seu sector. 

4. L’increment de les hores de pràctiques a empreses planteja
**problemes
en trobar llocs addicionals**
on poder fer la FCT amb garanties, en poder dur a terme la consecució
d’aquestes en els terminis establerts, i propicia l’augment de
les tasques burocràtiques i administratives i en dificulta les de
control per part del professorat. 

5. La reforma representa una pèrdua de capacitat de maniobra dels
instituts per adaptar la distribució de l’horari a la seva
realitat social i organitzativa, i amb la pèrdua de les hores de
lliure disposició es perd la capacitat d’innovació i de renovació
periòdica dels currículums a cada moment. **No
veiem viable en molts casos la pretesa igualació de continguts
curriculars**,
que l’únic que farà és restar-nos adaptabilitat i
encorsetar-nos, i que, en cas d’aplicació, hauria d’anar lligat
a converses i renovacions de continguts permanents, consensuats de
forma democràtica pels diferents centres. 

6. **No
es garanteixen que les hores de professorat puguin mantenir-se en un
futur**,
posant en joc l’equivalent en hores acadèmiques suprimides, més
d’un 5%, al desnaturalitzar-lo de la feina docent que li és
pròpia, per anar-lo substituint en càrrecs d’observador, de
comercial, i de visitador casual de les empreses. 

7. Que la gran majoria de **les
empreses participants en el nou model, no podran assegurar prou
tutorització i que l’aprenentatge aconseguit no sigui parcial,
específic,**
i destinat a la consecució urgent de les necessitats de cada
companyia. No creiem que l’alumnat aprengui el mateix a l’empresa
que a l’escola i, per tant, no poden substituir-se unes hores per
les altres.

8. **Continuaran
havent disfuncions organitzatives al mantenir dos models educatius en
paral·lel**.
No creiem que l’esforç d’equiparació de condicions hagi de
recaure tan sols en la part educativa, i, que en cas d’aplicació
de la reforma, la part empresarial hauria d’equiparar les
pràctiques duals i les de formació en centre de treball, posant de
la seva part una remuneració per a les FCT i assegurant **que
l’alumnat en pràctiques no es converteix en mà d’obra barata.**

9. Percebem amb l’experiència que tenim amb dual, per la priorització
que en fa la Generalitat de Catalunya per fomentar-la, i els
processos selectius de les empreses, que aplicar **els
canvis de la resolució portarà també a que es produeixi una
separació entre l’alumnat que opti per una modalitat i una altra,**
que el segregui en estudiants de primera i de segona classe, i que,
per tant, es renunciï a la funció d’equiparació que hauria de
fer l’escola pública. 

10. Finalment, i després de tots els punts anteriors, **considerem
que aquesta reforma no va encaminada en millorar el sistema de
formació reglada,**
sinó tan sols a fomentar i facilitar la transició encoberta de la
formació professional ordinària tradicional cap al model dual,
plantejant ja, aquesta, altres problemàtiques que caldria abordar.

Volem
deixar clar que tot això s’està fent tenint com a referència
l’alternança dual, sobre la que no s’ha fet cap valoració de
l’experiència portada a terme fins el moment, i no entenem perquè
s’està intensificant la promoció d’un model que no té cap
tipus de control ni avaluació rigorosa ni científica.